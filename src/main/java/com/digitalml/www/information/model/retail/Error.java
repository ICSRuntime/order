package com.digitalml.www.information.model.retail;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Error:
{
  "type": "object",
  "properties": {
    "code": {
      "type": "integer",
      "format": "int32"
    },
    "name": {
      "type": "string"
    },
    "description": {
      "type": "string"
    }
  }
}
*/

public class Error {

	@Size(max=1)
	private Integer code;

	@Size(max=1)
	private String name;

	@Size(max=1)
	private String description;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    
	    name = null;
	    description = null;
	}
	public Integer getCode() {
		return code;
	}
	
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
}