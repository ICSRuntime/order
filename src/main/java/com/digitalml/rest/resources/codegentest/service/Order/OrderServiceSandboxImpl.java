package com.digitalml.rest.resources.codegentest.service.Order;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import com.digitalml.rest.resources.codegentest.service.OrderService;

import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.lang.StringUtils;

/**
 * Sandbox implementation for: Order
 * Access and update of order information.
 *
 * @author admin
 * @version 1.0
 *
 */

public class OrderServiceSandboxImpl extends OrderService {
	

    public GetASpecificOrderCurrentStateDTO getaspecificorderUseCaseStep1(GetASpecificOrderCurrentStateDTO currentState) {
    

        GetASpecificOrderReturnStatusDTO returnStatus = new GetASpecificOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificOrderCurrentStateDTO getaspecificorderUseCaseStep2(GetASpecificOrderCurrentStateDTO currentState) {
    

        GetASpecificOrderReturnStatusDTO returnStatus = new GetASpecificOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificOrderCurrentStateDTO getaspecificorderUseCaseStep3(GetASpecificOrderCurrentStateDTO currentState) {
    

        GetASpecificOrderReturnStatusDTO returnStatus = new GetASpecificOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificOrderCurrentStateDTO getaspecificorderUseCaseStep4(GetASpecificOrderCurrentStateDTO currentState) {
    

        GetASpecificOrderReturnStatusDTO returnStatus = new GetASpecificOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificOrderCurrentStateDTO getaspecificorderUseCaseStep5(GetASpecificOrderCurrentStateDTO currentState) {
    

        GetASpecificOrderReturnStatusDTO returnStatus = new GetASpecificOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public GetASpecificOrderCurrentStateDTO getaspecificorderUseCaseStep6(GetASpecificOrderCurrentStateDTO currentState) {
    

        GetASpecificOrderReturnStatusDTO returnStatus = new GetASpecificOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public FindOrderCurrentStateDTO findorderUseCaseStep1(FindOrderCurrentStateDTO currentState) {
    

        FindOrderReturnStatusDTO returnStatus = new FindOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindOrderCurrentStateDTO findorderUseCaseStep2(FindOrderCurrentStateDTO currentState) {
    

        FindOrderReturnStatusDTO returnStatus = new FindOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindOrderCurrentStateDTO findorderUseCaseStep3(FindOrderCurrentStateDTO currentState) {
    

        FindOrderReturnStatusDTO returnStatus = new FindOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindOrderCurrentStateDTO findorderUseCaseStep4(FindOrderCurrentStateDTO currentState) {
    

        FindOrderReturnStatusDTO returnStatus = new FindOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindOrderCurrentStateDTO findorderUseCaseStep5(FindOrderCurrentStateDTO currentState) {
    

        FindOrderReturnStatusDTO returnStatus = new FindOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public FindOrderCurrentStateDTO findorderUseCaseStep6(FindOrderCurrentStateDTO currentState) {
    

        FindOrderReturnStatusDTO returnStatus = new FindOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public UpdateOrderCurrentStateDTO updateorderUseCaseStep1(UpdateOrderCurrentStateDTO currentState) {
    

        UpdateOrderReturnStatusDTO returnStatus = new UpdateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateOrderCurrentStateDTO updateorderUseCaseStep2(UpdateOrderCurrentStateDTO currentState) {
    

        UpdateOrderReturnStatusDTO returnStatus = new UpdateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateOrderCurrentStateDTO updateorderUseCaseStep3(UpdateOrderCurrentStateDTO currentState) {
    

        UpdateOrderReturnStatusDTO returnStatus = new UpdateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateOrderCurrentStateDTO updateorderUseCaseStep4(UpdateOrderCurrentStateDTO currentState) {
    

        UpdateOrderReturnStatusDTO returnStatus = new UpdateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateOrderCurrentStateDTO updateorderUseCaseStep5(UpdateOrderCurrentStateDTO currentState) {
    

        UpdateOrderReturnStatusDTO returnStatus = new UpdateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateOrderCurrentStateDTO updateorderUseCaseStep6(UpdateOrderCurrentStateDTO currentState) {
    

        UpdateOrderReturnStatusDTO returnStatus = new UpdateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public UpdateOrderCurrentStateDTO updateorderUseCaseStep7(UpdateOrderCurrentStateDTO currentState) {
    

        UpdateOrderReturnStatusDTO returnStatus = new UpdateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public ReplaceOrderCurrentStateDTO replaceorderUseCaseStep1(ReplaceOrderCurrentStateDTO currentState) {
    

        ReplaceOrderReturnStatusDTO returnStatus = new ReplaceOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceOrderCurrentStateDTO replaceorderUseCaseStep2(ReplaceOrderCurrentStateDTO currentState) {
    

        ReplaceOrderReturnStatusDTO returnStatus = new ReplaceOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceOrderCurrentStateDTO replaceorderUseCaseStep3(ReplaceOrderCurrentStateDTO currentState) {
    

        ReplaceOrderReturnStatusDTO returnStatus = new ReplaceOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceOrderCurrentStateDTO replaceorderUseCaseStep4(ReplaceOrderCurrentStateDTO currentState) {
    

        ReplaceOrderReturnStatusDTO returnStatus = new ReplaceOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceOrderCurrentStateDTO replaceorderUseCaseStep5(ReplaceOrderCurrentStateDTO currentState) {
    

        ReplaceOrderReturnStatusDTO returnStatus = new ReplaceOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceOrderCurrentStateDTO replaceorderUseCaseStep6(ReplaceOrderCurrentStateDTO currentState) {
    

        ReplaceOrderReturnStatusDTO returnStatus = new ReplaceOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public ReplaceOrderCurrentStateDTO replaceorderUseCaseStep7(ReplaceOrderCurrentStateDTO currentState) {
    

        ReplaceOrderReturnStatusDTO returnStatus = new ReplaceOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public CreateOrderCurrentStateDTO createorderUseCaseStep1(CreateOrderCurrentStateDTO currentState) {
    

        CreateOrderReturnStatusDTO returnStatus = new CreateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateOrderCurrentStateDTO createorderUseCaseStep2(CreateOrderCurrentStateDTO currentState) {
    

        CreateOrderReturnStatusDTO returnStatus = new CreateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateOrderCurrentStateDTO createorderUseCaseStep3(CreateOrderCurrentStateDTO currentState) {
    

        CreateOrderReturnStatusDTO returnStatus = new CreateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateOrderCurrentStateDTO createorderUseCaseStep4(CreateOrderCurrentStateDTO currentState) {
    

        CreateOrderReturnStatusDTO returnStatus = new CreateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateOrderCurrentStateDTO createorderUseCaseStep5(CreateOrderCurrentStateDTO currentState) {
    

        CreateOrderReturnStatusDTO returnStatus = new CreateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateOrderCurrentStateDTO createorderUseCaseStep6(CreateOrderCurrentStateDTO currentState) {
    

        CreateOrderReturnStatusDTO returnStatus = new CreateOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public DeleteOrderCurrentStateDTO deleteorderUseCaseStep1(DeleteOrderCurrentStateDTO currentState) {
    

        DeleteOrderReturnStatusDTO returnStatus = new DeleteOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteOrderCurrentStateDTO deleteorderUseCaseStep2(DeleteOrderCurrentStateDTO currentState) {
    

        DeleteOrderReturnStatusDTO returnStatus = new DeleteOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteOrderCurrentStateDTO deleteorderUseCaseStep3(DeleteOrderCurrentStateDTO currentState) {
    

        DeleteOrderReturnStatusDTO returnStatus = new DeleteOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteOrderCurrentStateDTO deleteorderUseCaseStep4(DeleteOrderCurrentStateDTO currentState) {
    

        DeleteOrderReturnStatusDTO returnStatus = new DeleteOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public DeleteOrderCurrentStateDTO deleteorderUseCaseStep5(DeleteOrderCurrentStateDTO currentState) {
    

        DeleteOrderReturnStatusDTO returnStatus = new DeleteOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public CreateSimpleOrderCurrentStateDTO createSimpleOrderUseCaseStep1(CreateSimpleOrderCurrentStateDTO currentState) {
    

        CreateSimpleOrderReturnStatusDTO returnStatus = new CreateSimpleOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateSimpleOrderCurrentStateDTO createSimpleOrderUseCaseStep2(CreateSimpleOrderCurrentStateDTO currentState) {
    

        CreateSimpleOrderReturnStatusDTO returnStatus = new CreateSimpleOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateSimpleOrderCurrentStateDTO createSimpleOrderUseCaseStep3(CreateSimpleOrderCurrentStateDTO currentState) {
    

        CreateSimpleOrderReturnStatusDTO returnStatus = new CreateSimpleOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateSimpleOrderCurrentStateDTO createSimpleOrderUseCaseStep4(CreateSimpleOrderCurrentStateDTO currentState) {
    

        CreateSimpleOrderReturnStatusDTO returnStatus = new CreateSimpleOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateSimpleOrderCurrentStateDTO createSimpleOrderUseCaseStep5(CreateSimpleOrderCurrentStateDTO currentState) {
    

        CreateSimpleOrderReturnStatusDTO returnStatus = new CreateSimpleOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

    public CreateSimpleOrderCurrentStateDTO createSimpleOrderUseCaseStep6(CreateSimpleOrderCurrentStateDTO currentState) {
    

        CreateSimpleOrderReturnStatusDTO returnStatus = new CreateSimpleOrderReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: Access and update of order information.");
	    currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


	/**
	 * Creates and returns a {@link Method} object using reflection and handles the possible exceptions.
	 * <br/>
	 * Aids with calling the process step method based on the outcome of the control logic
	 * 
	 * @param methodName
	 * @param clazz
	 * @return
	 */
	private Method getMethod(String methodName, Class clazz) {
		Method method = null;
		try {
			method = OrderService.class.getMethod(methodName, clazz);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return method;
	}
	
	/**
	 * For use when calling provider systems.
	 * <p>
	 * TODO: Implement security logic here
	 */
	protected SecurityContext securityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}