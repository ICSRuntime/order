package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class OrderTests {

	@Test
	public void testResourceInitialisation() {
		OrderResource resource = new OrderResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationGetASpecificOrderNoSecurity() {
		OrderResource resource = new OrderResource();
		resource.setSecurityContext(null);

		Response response = resource.getaspecificorder(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationFindOrderNoSecurity() {
		OrderResource resource = new OrderResource();
		resource.setSecurityContext(null);

		Response response = resource.findorder(0, 0);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationUpdateOrderNoSecurity() {
		OrderResource resource = new OrderResource();
		resource.setSecurityContext(null);

		Response response = resource.updateorder(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationReplaceOrderNoSecurity() {
		OrderResource resource = new OrderResource();
		resource.setSecurityContext(null);

		Response response = resource.replaceorder(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateOrderNoSecurity() {
		OrderResource resource = new OrderResource();
		resource.setSecurityContext(null);

		Response response = resource.createorder(new com.digitalml.rest.resources.codegentest.Request());
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationDeleteOrderNoSecurity() {
		OrderResource resource = new OrderResource();
		resource.setSecurityContext(null);

		Response response = resource.deleteorder(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationCreateSimpleOrderNoSecurity() {
		OrderResource resource = new OrderResource();
		resource.setSecurityContext(null);

		Response response = resource.createSimpleOrder(null, null, null);
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}