package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Order.OrderServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.OrderService.CreateOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.CreateOrderReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class CreateOrderTests {

	@Test
	public void testOperationCreateOrderBasicMapping()  {
		OrderServiceDefaultImpl serviceDefaultImpl = new OrderServiceDefaultImpl();
		CreateOrderInputParametersDTO inputs = new CreateOrderInputParametersDTO();
		inputs.setRequest(new com.digitalml.rest.resources.codegentest.Request());
		CreateOrderReturnDTO returnValue = serviceDefaultImpl.createorder(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}