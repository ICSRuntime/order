package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Order.OrderServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.OrderService.GetASpecificOrderInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.OrderService.GetASpecificOrderReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetASpecificOrderTests {

	@Test
	public void testOperationGetASpecificOrderBasicMapping()  {
		OrderServiceDefaultImpl serviceDefaultImpl = new OrderServiceDefaultImpl();
		GetASpecificOrderInputParametersDTO inputs = new GetASpecificOrderInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		GetASpecificOrderReturnDTO returnValue = serviceDefaultImpl.getaspecificorder(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}